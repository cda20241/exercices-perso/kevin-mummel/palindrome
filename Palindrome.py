import unidecode


def phrase_propre(p_phrase: str) -> str:
    """
    La fonction rend une phrase exclusivement alphabetique, en minuscule et sans espace
    """
    phrase_propre = ''.join(lettre.lower() for lettre in p_phrase if lettre.isalpha())
    # concaténer la chaine de caractère en minuscule dans p_phrase si ce sont des caractères alphabetique
    return unidecode.unidecode(phrase_propre)
    # retourne la chaine de caractère sans accents


def est_palindrome(p_phrase: str) -> bool:
    """
    La fonction compare la phrase propre avec son inverse
    """
    phrase_min = phrase_propre(p_phrase)
    # la variable phrase_min recupère la valeur de la phrase propre
    return phrase_min == phrase_min[::-1]
    # Retourne si la phrase min est egal a son inverse


if __name__ == '__main__':
    phrase = input("Entrez une phrase svp : ")
    # Demande a l'utilisateur de taper une phrase
    if est_palindrome(phrase):
    # Si c'est un palindrome on imprime ci-dessous
        print("La phrase est un palindrome.")
    else:
    # Sinon on imprime ci-dessous
        print("La phrase n'est pas un palindrome.")
